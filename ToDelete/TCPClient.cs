﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ToDelete
{
    public struct Packet
    {
        public PacketServiceData packetServiceData;
        public byte[] bData;

        private static ushort test = 2;
    }

    public struct PacketServiceData
    {
        public byte bAdressSender;
        public byte bAdressReceiver;
        public byte bCode;
        public byte bCounter;
        public ushort usLengthInform;
    }

    //public struct StructSupprFWS
    //{
    //    public int iFreq;
    //    public byte bModulation;
    //    public byte bDeviation;
    //    public byte bManipulation;
    //    public byte bDuration;
    //    public byte bPrioritet;
    //    public byte bThreshold;
    //    public short sBearing;
    //}

    public class TCPClient
    {
        #region variables

        private Thread thrRead;
        public NetworkStream streamClient;
        private TcpClient tcpClient;
        private byte bAdrOwn = 0;
        private byte bAdrOpponent = 0;
        private byte COUNTER_CMD = 0;

        #endregion

        static int LEN_HEAD = 6;

        #region constants
        const byte CONNECTION_REQUEST_CODE = 9;
        const byte TEXT_MESSAGE_CODE = 10;
        const byte TEXT_MESSAGE_APPROVED_CMD_CODE = 11;
        const byte EXEC_BEAR_CODE = 12;
        const byte FRIEQUENCIES_FOR_SUPPRESSION_CODE = 13;
        const byte COORD_REQUEST_CODE = 14;

        const byte VALUE_OF_MESSAGE_APPROVED_TEXT_CMD = 0;

        #endregion



        //************** EVENT **************//      
        public delegate void ConnectEventHandler();
        public event ConnectEventHandler OnConnectNet;
        public event ConnectEventHandler OnDisconnectNet;

        public delegate void ByteEventHandler(byte[] bByte);
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public delegate void CmdConnectionRequestEventHandler(byte bRegime);
        public event CmdConnectionRequestEventHandler OnConnectionRequest;

        public delegate void CmdTextEventHandler(string str);
        public event CmdTextEventHandler OnTextCmd;

        public delegate void CmdTestEventHandler(short str);
        public event CmdTestEventHandler OnReceiveTestCmd;


        public delegate void CmdShortREListEventHandler(List<int> shortRECol);
        public event CmdShortREListEventHandler OnReceiveShortREColCmd;

        public delegate void CmdShortIMListEventHandler(List<int> shortIMCol);
        public event CmdShortIMListEventHandler OnReceiveShortIMColCmd;


        // connect to server
        protected virtual void ConnectNet()
        {
            if (OnConnectNet != null)
            {
                OnConnectNet();//Raise the event
            }
        }

        // disconnect 
        protected virtual void DisconnectNet()
        {
            if (OnDisconnectNet != null)
            {
                OnDisconnectNet();
            }
        }

        // read array of byte
        protected virtual void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);
            }

        }

        // write array of byte
        protected virtual void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);
            }
        }

        // read connection request
        protected virtual void ConnectionRequest(byte bRegime)
        {
            if (OnConnectionRequest != null)
            {
                OnConnectionRequest(bRegime);
            }
        }

        // read text mesage
        protected virtual void TextCmd(string str)
        {
            if (OnTextCmd != null)
            {
                OnTextCmd(str);
            }
        }

        protected virtual void ReceiveTestCmd(short str)
        {
            if (OnReceiveTestCmd != null)
            {
                OnReceiveTestCmd(str);
            }
        }

        protected virtual void ReceiveShortREColCmd(List<int> shortCol)
        {
            if (OnReceiveShortREColCmd != null)
            {
                OnReceiveShortREColCmd(shortCol);
            }
        }

        protected virtual void ReceiveShortIMColCmd(List<int> shortCol)
        {
            if (OnReceiveShortIMColCmd != null)
            {
                OnReceiveShortIMColCmd(shortCol);
            }
        }





        // constructor
        public TCPClient(byte AdrOwn, byte AdrOpponent)
        {
            bAdrOwn = AdrOwn;
            bAdrOpponent = AdrOpponent;
        }


        // connect to server
        public bool Connect(string strIPServer, int iPortServer)
        {
            // if there is client
            if (tcpClient != null)
            {
                tcpClient.Close();
            }

            // create client
            tcpClient = new TcpClient();

            try
            {
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (Exception)
            {
                //generate event
                DisconnectNet();
                // logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                return false;
            }


            if (tcpClient.Connected)
            {
                // create stream for client
                if (streamClient != null)
                    streamClient = null;
                streamClient = tcpClient.GetStream();

                // destroy thread for reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // create thread for reading
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }

                catch (System.Exception)
                {

                    // generate event
                    DisconnectNet();
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    return false;

                }
            }

            ConnectNet();
            return true;
        }


        public void Disconnect()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                    streamClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (thrRead != null)
            {
                // destroy thread for reading
                try
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // generate event
            DisconnectNet();
        }

        //private static Logger logger = LogManager.GetCurrentClassLogger();

        //private void ReadData()
        //{

        //    Packet packet = new Packet();

        //    int iReadLength = 0;
        //    int currSize = 0;
        //    while (true)
        //    {
        //        try
        //        {
        //            byte[] bRead = null;
        //            byte[] buffer = null;
        //            byte[] bData = null;

        //            Array.Resize(ref buffer, LEN_HEAD);
        //            Array.Resize(ref bRead, 0);
        //            currSize = 0;
        //            iReadLength = streamClient.Read(buffer, 0, LEN_HEAD);
        //            //ReadByte(buffer);
        //            ushort realMessageLength = BitConverter.ToUInt16(buffer, 4);

        //            Array.Resize(ref bRead, realMessageLength + LEN_HEAD);
        //            Array.Resize(ref buffer, realMessageLength + LEN_HEAD);
        //            //new
        //            Array.Copy(buffer, 0, bRead, 0, LEN_HEAD);
        //            currSize += iReadLength;

        //            while (currSize < bRead.Length)
        //            {
        //                iReadLength = streamClient.Read(buffer, 0, bRead.Length - currSize);

        //                Array.Copy(buffer, 0, bRead, currSize, iReadLength);
        //                currSize += iReadLength;

        //                if (iReadLength == 0)
        //                {
        //                    Disconnect();
        //                }
        //            }

        //            if (bRead.Length > 0)
        //            {
        //                //// событие приема байт
        //                ReadByte(bRead);

        //                try
        //                {
        //                    if (bRead.Length >= LEN_HEAD)
        //                    {

        //                        packet.packetServiceData.bAdressSender = bRead[0];
        //                        packet.packetServiceData.bAdressReceiver = bRead[1];
        //                        packet.packetServiceData.bCode = bRead[2];
        //                        packet.packetServiceData.bCounter = bRead[3];
        //                        packet.packetServiceData.usLengthInform = BitConverter.ToUInt16(bRead, 4);

        //                        Array.Resize(ref bData, bRead.Length - LEN_HEAD);
        //                        Array.Copy(bRead, LEN_HEAD, bData, 0, bRead.Length - LEN_HEAD);
        //                        packet.bData = bData;

        //                        switch (packet.packetServiceData.bCode)
        //                        {
        //                            case CONNECTION_REQUEST_CODE:
        //                                byte bRegime = packet.bData[0];
        //                                ConnectionRequest(bRegime);

        //                                break;

        //                            case TEXT_MESSAGE_CODE:

        //                                string str = Encoding.GetEncoding(1251).GetString(packet.bData);
        //                                TextCmd(str);

        //                                // SendConfirmText(VALUE_OF_MESSAGE_APPROVED_TEXT_CMD);
        //                                break;



        //                            case 1:

        //                                // событие приема байт
        //                                //ReadByte(packet.bData);
        //                                ////while(cmd.bData)
        //                                ////{
        //                                ////    short test = BitConverter.ToInt16(bData, 0);
        //                                ////    OnReceiveTestCmd(test);
        //                                ////}
        //                                //List<ushort> testListRE = new List<ushort>();
        //                                //List<ushort> testListIM = new List<ushort>();

        //                                ////for (int i = 0; i * 4 + 2 < resultBData.Count(); ++i)
        //                                //for (int i = 0; i + 4 < bData.Count(); i += 4)
        //                                //{

        //                                //    OnReceiveTestCmd(BitConverter.ToInt16(bData.ToArray(), i));
        //                                //    OnReceiveTestCmd(BitConverter.ToInt16(bData.ToArray(), i + 2));
        //                                //}

        //                                break;

        //                            default:
        //                                break;
        //                        }


        //                    }

        //                }
        //                catch (System.Exception )
        //                {
        //                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
        //                }

        //            }
        //        }

        //        catch (System.Exception )
        //        {
        //            //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
        //            DisconnectNet();
        //            return;
        //        }
        //    }
        //}



        private void ReadData()
        {


            byte[] dataBuffer = new byte[2048];
            byte[] bRead = null;
            int iReadLength = 0;

            int curRe;
            int curIm;

            List<int> testListRE = new List<int>();
            List<int> testListIM = new List<int>();
            while (true)
            {
                try
                {
                    //List<byte> testList = new List<byte>();
                    //    int numBytesRead;
                    //    //while ((numBytesRead = streamClient.Read(data, 0, data.Length)) > 0)
                    //    //testList.AddRange(data);

                    iReadLength = streamClient.Read(dataBuffer, 0, dataBuffer.Length);

                    Array.Resize(ref bRead, iReadLength);
                    Array.Copy(dataBuffer, 0, bRead, 0, iReadLength);

                    //ushort realMessageLength = BitConverter.ToUInt16(buffer, 4);

                    //Array.Resize(ref bRead, dataBuffer.Length + LEN_HEAD);
                    ////new
                    //Array.Copy(buffer, 0, bRead, 0, LEN_HEAD);
                    //currSize += iReadLength;

                    //while (currSize < bRead.Length)
                    //{
                    //    iReadLength = streamClient.Read(buffer, 0, bRead.Length - currSize);

                    //    Array.Copy(buffer, 0, bRead, currSize, iReadLength);
                    //    currSize += iReadLength;

                    if (iReadLength == 0)
                    {
                        Disconnect();
                    }


                    if (bRead.Length >= 4)
                    {
                        //// событие приема байт
                        ReadByte(bRead);

                        try
                        {
                            if (dataBuffer.Length % 4 == 0)
                            {
                                //for (int i = 0; i * 4 + 2 < resultBData.Count(); ++i)
                                for (int i = 0; i + 4 <= bRead.Count(); i += 4)
                                {
                                    curRe = (BitConverter.ToInt16(bRead.ToArray(), i)) >> 2;
                                    curIm = (BitConverter.ToInt16(bRead.ToArray(), i + 2)) >> 2;
                                    testListRE.Add(curRe);
                                    testListIM.Add(curIm);
                                }
                            }
                            else
                            {
                                int correction = dataBuffer.Length % 4;
                                for (int i = 0; i + 4 <= dataBuffer.Count() - correction; i += 4)
                                {
                                    testListRE.Add(BitConverter.ToInt16(bRead.ToArray(), i) >> 2 );
                                    testListIM.Add(BitConverter.ToInt16(bRead.ToArray(), i + 2) >> 2);
                                }
                            }
                            ReceiveShortREColCmd(testListRE);
                            ReceiveShortIMColCmd(testListIM);

                            testListRE.Clear();
                            testListIM.Clear();
                        }
                        catch (System.Exception)
                        {
                            //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                        }

                    }
                }

                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    Disconnect();
                    return;
                }
            }
        }

        private void SendData(Packet packetToSend)
        {
            byte[] bSend;
            byte[] bHead = Serializer.StructToByteArray(packetToSend.packetServiceData);
            if (packetToSend.bData != null)
            {
                bSend = new byte[bHead.Length + packetToSend.bData.Length];
                Array.Copy(bHead, 0, bSend, 0, bHead.Length);
                Array.Copy(packetToSend.bData, 0, bSend, LEN_HEAD, packetToSend.bData.Length);
            }
            else
                bSend = new byte[bHead.Length];
            Array.Copy(bHead, 0, bSend, 0, bHead.Length);


            if (COUNTER_CMD == 255)
            {
                COUNTER_CMD = 0;
            }

            WriteData(bSend);

        }

        private bool WriteData(byte[] bSend)
        {
            try
            {
                streamClient.Write(bSend, 0, bSend.Length);
                OnWriteByte(bSend);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public void SendText(string strText)
        {
            try
            {
                Packet packet = new Packet();
                packet.bData = null;
                packet.bData = Encoding.GetEncoding(1251).GetBytes(strText);
                packet.packetServiceData.bAdressSender = bAdrOwn;
                packet.packetServiceData.bAdressReceiver = bAdrOpponent;
                packet.packetServiceData.bCode = 1;
                packet.packetServiceData.bCounter = COUNTER_CMD++;
                packet.packetServiceData.usLengthInform = Convert.ToUInt16(packet.bData.Length);
                SendData(packet);
            }
            catch (System.Exception)
            {
                //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }


        public void SendBytesVithoutPacket(string strText)
        {
            try
            {
                //byte[] ba = Encoding.Default.GetBytes("sample");
                //var hexString = BitConverter.ToString(ba);
                //hexString = hexString.Replace("-", "");

                ////bSend = new byte[packetToSend.bData.Length];
                ////byte[] bSend = Encoding.GetEncoding(1251).GetBytes(strText);
                //byte[] bSend = Encoding.Default.GetBytes(strText);
                byte[] bSend = Serializer.StringToByteArray(strText);
                WriteData(bSend);
            }
            catch (System.Exception)
            {
                //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }
    }
}
