﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDelete
{
    [ProtoContract(SkipConstructor = true)]
    public class Settings
    {
        [ProtoMember(1)]
        public string IP { get; set; }

        [ProtoMember(2)]
        public int Port { get; set; }

        [ProtoMember(3)]
        public bool asServer { get; set; }

        public Settings()
        {

        }
    }
}
