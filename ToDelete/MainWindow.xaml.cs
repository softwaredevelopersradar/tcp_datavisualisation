﻿using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.EventMarkers;
using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using PublicColor = System.Windows.Media.Color;

namespace ToDelete
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        static PublicColor backgroundColor = PublicColor.FromArgb(255, 30, 30, 30);
        static PublicColor titleColor = PublicColor.FromArgb(255, 249, 202, 3);

        private TCPClient Client = new TCPClient(2, 1);
        public Settings testSettings;
        private string nameOfBinFile = "settings.bin";

        public PointLineSeries pointLineSeries1;//= new PointLineSeries();
        public PointLineSeries pointLineSeries2;//= new PointLineSeries();

        #region fedya_s
        private const int minCharts = 1;
        private const int maxCharts = 10;
        private const int minDelay = 1;
        private const int maxDelay = 1_000;
        private const int minPoints = 10;
        private const int maxPoints = 1_000_000;
        private const int minYValue = 0;
        private const int maxYValue = 10;

        private int _numberOfCharts = 5;
        private int _delay = 50;
        private int _numberOfPoints = 100;
        //private ColorPalette _palette;

        private CancellationTokenSource _tokenSource;
        private Task workTask;
        private Task workTask2;

        private List<int> curFirstList;
        private List<int> curSecondList;

        #endregion

        List<byte> curListOfBytes = new List<byte>();

        private static List<byte> bDataList = new List<byte>();
        private static List<byte> listOfBytes = new List<byte>();
        private static byte[] headderArray = new byte[] { 2, 1, 1, 0 };
        private static byte[] bLength = new byte[2];

        private static ushort realMessageLength = 0;
        private static bool testFlag = false;
        private static bool endOfSequence = false;

        #region variables
        private byte bAdrOpponent = 2;
        private byte bAdrOwn = 1;
        private static byte COUNTER_CMD = 0;
        private static byte COUNTER_CMD_OWN = 0;
        private static bool status = false;


        #endregion

        static int LEN_HEAD = 6;

        #region constants
        const byte DATA_REQUEST_CMD_CODE = 1;

        #endregion


        public MainWindow()
        {
            //Set Deployment Key for Arction components             
            string deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            //Set Deployment Key for semi-bindable chart, if you use it 
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();


            LoadSettings();
            //Init();

            ////string portName = "COM20";
            ////int baudRate = 9600;
            //SetPort(portName, baudRate);
            //bool rezult = OpenPort();
            //Task.Run(() => ReadData());

            checkBoxLogX.IsChecked = false;
            checkBoxLogX.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent));

            checkBoxLogY.IsChecked = false;
            checkBoxLogY.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent));

            _tokenSource = new CancellationTokenSource();

            InitializeChart();
            _tokenSource.Cancel();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            SaveSettings();
            Client.Disconnect();
        }

        private void InitializeChart()
        {
            Chart.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.Pan;
            Chart.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;
            Chart.ViewXY.XAxes[0].MouseInteraction = false;
            Chart.ViewXY.YAxes[0].MouseInteraction = false;

            Chart2.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.Pan;
            Chart2.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;
            Chart2.ViewXY.XAxes[0].MouseInteraction = false;
            Chart2.ViewXY.YAxes[0].MouseInteraction = false;


            var pls = new PointLineSeries();
            pls.PointsVisible = false;
            pls.MouseInteraction = false;
            //pls.LineStyle.Color = _palette.Colors[i];
            Chart.ViewXY.PointLineSeries.Add(pls);

            var pls2 = new PointLineSeries();
            pls.PointsVisible = false;
            pls.MouseInteraction = false;
            //pls.ShowInLegendBox = false;
            //pls.LineStyle.Color = _palette.Colors[i];
            Chart2.ViewXY.PointLineSeries.Add(pls2);



            //Chart.ViewXY.XAxes[0].RangeChanged += (sender, args) =>
            //{
            //    if (args.NewMin < 0)
            //        args.NewMin = 0;
            //    if (args.NewMax > _numberOfPoints)
            //        args.NewMax = _numberOfPoints - 1;
            //    Chart.ViewXY.XAxes[0].SetRange(args.NewMin, args.NewMax);
            //};
            //Chart.ViewXY.YAxes[0].RangeChanged += (sender, args) =>
            //{
            //    if (args.NewMin < minYValue - 5)
            //        args.NewMin = minYValue - 5;
            //    if (args.NewMax > maxYValue + 4)
            //        args.NewMax = maxYValue + 4;
            //    Chart.ViewXY.YAxes[0].SetRange(args.NewMin, args.NewMax);
            //};
            //Chart.ViewXY.YAxes[0].SetRange(minYValue - 5, maxYValue + 4);
        }


        private async Task WorkTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {

                Client.SendBytesVithoutPacket("1f a3");
                //Task.Run(() => Client.SendBytesVithoutPacket("1f a3"));
                //if (curFirstList != null)
                //{
                //    var newData = curFirstList;
                //    var newData2 = curSecondList;
                //    Dispatcher?.Invoke(() =>
                //    {
                //        ShowData(newData, newData2);
                //    });

                //}
                await Task.Delay(_delay);
            }
        }

        private void ShowData(List<int> inputData, List<int> inputData2)
        {
            try
            {
                // oneDrawCycleWatch.Restart();
                if (inputData == null)
                    return;
                Chart.BeginUpdate();

                Chart.ViewXY.PointLineSeries[0].Points = new SeriesPoint[inputData.Count];
                for (int j = 0; j < inputData.Count; j++)
                {
                    Chart.ViewXY.PointLineSeries[0].Points[j] = new SeriesPoint(j, inputData[j]);
                }

                Chart.EndUpdate();

                Chart2.BeginUpdate();

                Chart2.ViewXY.PointLineSeries[0].Points = new SeriesPoint[inputData2.Count];
                for (int j = 0; j < inputData2.Count; j++)
                {
                    Chart2.ViewXY.PointLineSeries[0].Points[j] = new SeriesPoint(j, inputData2[j]);
                }

                Chart2.EndUpdate();

                curFirstList = null;
                curSecondList = null;

                Chart.ViewXY.ZoomToFit();
                Chart2.ViewXY.ZoomToFit();

            }
            catch (Exception ex) { }
        }



        private void marker_MouseOverOff(object sender, MouseEventArgs e)
        {
            ((SeriesEventMarker)sender).Label.Visible = false;
        }

        private void marker_MouseOverOn(object sender, MouseEventArgs e)
        {
            ((SeriesEventMarker)sender).Label.Visible = true;
        }

        private void checkBoxLogX_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (Chart == null)
            {
                return;
            }

            if (checkBoxLogX.IsChecked == true)
            {
                Chart.ViewXY.XAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                Chart.ViewXY.XAxes[0].ScaleType = ScaleType.Linear;
            }


            if (Chart2 == null)
            {
                return;
            }

            if (checkBoxLogX.IsChecked == true)
            {
                Chart2.ViewXY.XAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                Chart2.ViewXY.XAxes[0].ScaleType = ScaleType.Linear;
            }

        }

        private void checkBoxLogY_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (Chart == null)
            {
                return;
            }

            if (checkBoxLogY.IsChecked == true)
            {
                Chart.ViewXY.YAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                Chart.ViewXY.YAxes[0].ScaleType = ScaleType.Linear;
            }


            if (Chart2 == null)
            {
                return;
            }

            if (checkBoxLogY.IsChecked == true)
            {
                Chart2.ViewXY.YAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                Chart2.ViewXY.YAxes[0].ScaleType = ScaleType.Linear;
            }

            //_chart.ViewXY.YAxes[0].SetRange(_chart.ViewXY.YAxes[0].Minimum, _chart.ViewXY.YAxes[0].Maximum);
        }

        private void buttonFitView_Click(object sender, RoutedEventArgs e)
        {
            Chart.ViewXY.ZoomToFit();
            Chart2.ViewXY.ZoomToFit();
            //_chart.ViewXY.ZoomToFit();
            //_chart2.ViewXY.ZoomToFit();
        }

        private void checkBoxNatural_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (Chart != null)
            {
                //Disable rendering, strongly recommended before updating chart properties
                Chart.BeginUpdate();

                if (checkBoxNatural.IsChecked == true)
                {
                    Chart.ViewXY.XAxes[0].LogBase = Math.E;
                    Chart.ViewXY.XAxes[0].LogLabelsType = LogLabelsType.LogE_MultiplesOfNeper;

                    Chart.ViewXY.YAxes[0].LogBase = Math.E;
                    Chart.ViewXY.YAxes[0].LogLabelsType = LogLabelsType.LogE_MultiplesOfNeper;
                }
                else
                {
                    Chart.ViewXY.XAxes[0].LogBase = 10;
                    Chart.ViewXY.XAxes[0].LogLabelsType = LogLabelsType.Regular;

                    Chart.ViewXY.YAxes[0].LogBase = 10;
                    Chart.ViewXY.YAxes[0].LogLabelsType = LogLabelsType.Regular;
                }
                //Allow chart rendering
                Chart.EndUpdate();
            }

            if (Chart2 != null)
            {
                //Disable rendering, strongly recommended before updating chart properties
                Chart2.BeginUpdate();

                if (checkBoxNatural.IsChecked == true)
                {
                    Chart2.ViewXY.XAxes[0].LogBase = Math.E;
                    Chart2.ViewXY.XAxes[0].LogLabelsType = LogLabelsType.LogE_MultiplesOfNeper;

                    Chart2.ViewXY.YAxes[0].LogBase = Math.E;
                    Chart2.ViewXY.YAxes[0].LogLabelsType = LogLabelsType.LogE_MultiplesOfNeper;
                }
                else
                {
                    Chart2.ViewXY.XAxes[0].LogBase = 10;
                    Chart2.ViewXY.XAxes[0].LogLabelsType = LogLabelsType.Regular;

                    Chart2.ViewXY.YAxes[0].LogBase = 10;
                    Chart2.ViewXY.YAxes[0].LogLabelsType = LogLabelsType.Regular;
                }
                //Allow chart rendering
                Chart2.EndUpdate();
            }
        }


        /// <summary>
        /// Dispose items in collection before and clear.
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="list">Collection</param>
#if WpfSemibindable
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#else
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#endif
        {
            if (list == null)
                return;

            foreach (IDisposable item in list)
            {
                if (item != null)
                    item.Dispose();
            }

            list.Clear();
        }



        private void Button_ConnectClick(object sender, RoutedEventArgs e)
        {
            try
            {
                InitClientEvents();
                ConectASClient();

                status = true;
                ConnectButton.Background = Brushes.Green;
                DisconnectButton.Background = Brushes.Gray;
            }
            catch (Exception exp)
            { }
        }

        private void InitClientEvents()
        {
            Client.OnConnectNet += Connected;
            Client.OnDisconnectNet += Disconnected;
            Client.OnReadByte += MathFunc;
        }


        private void ConectASClient()
        {
            string strIP;
            int testPortOperChannel;
            if (IPTextBox.Text.Equals(""))
            {
                strIP = "127.0.0.1";
                IPTextBox.Text = "127.0.0.1";
            }
            else
            {
                strIP = IPTextBox.Text;
            }

            if (PortTextBox.Text.Equals(""))
            {
                testPortOperChannel = 11111;
                PortTextBox.Text = testPortOperChannel.ToString();
            }
            else
            {
                testPortOperChannel = Convert.ToInt32(PortTextBox.Text);
            }
            Client.Connect(strIP, testPortOperChannel);
        }


        //whoud it be await?
        private void Button_Disconnect_Click(object sender, RoutedEventArgs e)
        {
            Client.OnConnectNet -= Connected;
            Client.OnDisconnectNet -= Disconnected;
            Client.OnReadByte -= MathFunc;

            status = false;

            Client.Disconnect();
            DisconnectButton.Background = Brushes.Red;
            ConnectButton.Background = Brushes.Gray;
        }


        public void MathFunc(IEnumerable<byte> resultBData)
        {
            //var w = new System.Diagnostics.Stopwatch();
            //w.Start();


            //resultBData.ToList().ForEach(item => Console.WriteLine(item.ToString()));

            List<int> testListRE = new List<int>();
            List<int> testListIM = new List<int>();
            int curRe;
            int curIm;

            for (int i = 0; i + 4 < resultBData.Count(); i += 4)
            {

                curRe = BitConverter.ToInt16(resultBData.ToArray(), i) >> 2;
                curIm = (BitConverter.ToInt16(resultBData.ToArray(), i + 2)) >> 2;

                testListRE.Add(curRe);
                testListIM.Add(curIm);
            }



            //CreateChart();
            curFirstList = testListRE;
            curSecondList = testListIM;
            WorkTask2(testListRE, testListIM);
            //UpateChart(testListRE);
            //UpDateChart2(testListIM);
            //Console.WriteLine($"{w.Elapsed.TotalMilliseconds}");
        }


        private async Task WorkTask2(List<int> testListRE, List<int> testListIM)
        {

            //await Task.Run(() => Client.SendBytesVithoutPacket("1f a3"));
            Dispatcher?.Invoke(() =>
            {
                ShowData(testListRE, testListIM);
            });

            await Task.Delay(_delay);
        }

        private void Button_set_counter_to_0_Click(object sender, RoutedEventArgs e)
        {
            COUNTER_CMD = 0;
            headderArray[3] = COUNTER_CMD;
        }

        private void Button_Send_Request_Click(object sender, RoutedEventArgs e)
        {
            //if (status)
            //{
            //    string strText = "1f a3";
            //    Client.SendText(strText);
            //}
            //else
            //    MessageBox.Show("подключение было завершено, повтотире попытку подключиться");
            //Task.Run(() => Client.SendBytesVithoutPacket("1f a3"));
            Client.SendBytesVithoutPacket("1f a3");
        }




        private void Connected()
        {
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.GreenYellow);
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Gray);
        }


        private void Disconnected()
        {
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Red);
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.Gray);
        }



        private void SaveSettings()
        {
            Settings testSettings = new Settings();
            testSettings.IP = IPTextBox.Text;
            testSettings.Port = int.Parse(PortTextBox.Text);


            using (var file = File.Create(nameOfBinFile))
            {
                ProtoBuf.Serializer.Serialize(file, testSettings);
            }
        }


        private void LoadSettings()
        {
            Settings curSettings = new Settings();

            if (File.Exists(nameOfBinFile))
            {
                using (var file = File.OpenRead(nameOfBinFile))
                {
                    curSettings = ProtoBuf.Serializer.Deserialize<Settings>(file);
                }
            }

            IPTextBox.Text = curSettings.IP;
            if (IPTextBox.Text == string.Empty)
                IPTextBox.Text = "192.168.0.11";
            PortTextBox.Text = curSettings.Port.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!_tokenSource.Token.IsCancellationRequested)
            {
                _tokenSource.Cancel();
                button.Content = "Start";
            }
            else
            {
                _tokenSource = new CancellationTokenSource();
                WorkTask(_tokenSource.Token);
                button.Content = "Stop";
            }
        }
    }
}
