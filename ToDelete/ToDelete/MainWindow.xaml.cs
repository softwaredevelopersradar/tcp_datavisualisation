﻿using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.EventMarkers;
using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using PublicColor = System.Windows.Media.Color;

namespace ToDelete
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LightningChartUltimate _chart = new LightningChartUltimate();
        private LightningChartUltimate _chart2 = new LightningChartUltimate();
        private LightningChartUltimate _chart3 = new LightningChartUltimate();

        private int numberOfCharts = 3;
        private LightningChartUltimate[] chartArray;
        //

        static int LEN_HEAD = 3;

        SeriesEventMarker prev;

        // for tests
        #region forTests
        private CancellationTokenSource _tokenSource;
        private CancellationTokenSource _TesttokenSource;
        private Task workTask;
        private const int minDelay = 1;
        private const int maxDelay = 1_000;

        private bool redyToMark = false;
        private const int minPoints = 10;
        private const int maxPoints = 1_000_000;
        private const int minYValue = 0;
        private const int maxYValue = 10;

        private int _delay = 50;
        private ushort _numberOfPoints = 2048;
        private Random _random = new Random();
        #endregion

        private TCPClient Client = new TCPClient(2, 1);



        public MainWindow()
        {
            //Set Deployment Key for Arction components     
            // Set it to work
            string deploymentKey = "";
            //Set Deployment Key for semi-bindable chart, if you use it 
            LightningChartUltimate.SetDeploymentKey(deploymentKey);

            _chart = null;
            _chart2 = null;
            _chart3 = null;
            InitializeComponent();

            CreateChart();
            CreateChart2();
            CreateChart3();
            chartArray = new LightningChartUltimate[] { _chart, _chart2, _chart3 };

            checkBoxLogX.IsChecked = false;
            checkBoxLogX.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent));

            checkBoxLogY.IsChecked = false;
            checkBoxLogY.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent));
        }



        /// <summary>
        /// Create charts
        /// </summary>
        private void CreateChart()
        {
            if (_chart != null)
                _chart.Dispose();
            // Create a new chart.
            _chart = new LightningChartUltimate();
            //Disable rendering, strongly recommended before updating chart properties
            _chart.BeginUpdate();

            //_chart.ChartName = "LogAxes chart";
            AxisX xAxis = _chart.ViewXY.XAxes[0];

            //Reduce memory usage and increase performance
            _chart.ViewXY.DropOldSeriesData = false;


            //Don't show legend box
            _chart.ViewXY.LegendBoxes[0].Visible = false;

            // Configure x-axis.
            //AxisX axisX = _chart.ViewXY.XAxes[0];
            xAxis.SetRange(0, 90);
            xAxis.ScrollMode = XAxisScrollMode.None;
            xAxis.ValueType = AxisValueType.Number;

            //Y-axis are layered and have common x-axis drawn
            _chart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Layered;

            //Remove existing y-axes
            foreach (AxisY yaxis in _chart.ViewXY.YAxes)
                yaxis.Dispose();
            _chart.ViewXY.YAxes.Clear();

            //Create new logaritmic y-axis and add to chart
            AxisY yAxis = new AxisY(_chart.ViewXY);
            yAxis.Title.DistanceToAxis = 30;
            yAxis.SetRange(0, 40);
            yAxis.MinorGrid.Visible = true;
            _chart.ViewXY.YAxes.Add(yAxis);

            //Create new PointLineSeries
            PointLineSeries pointLineSeries = new PointLineSeries(_chart.ViewXY, xAxis, yAxis);
            pointLineSeries.MouseInteraction = false;
            pointLineSeries.Title.Visible = false;
            pointLineSeries.LineStyle.Color = Colors.Yellow;
            pointLineSeries.Title.Color = pointLineSeries.LineStyle.Color;
            //pointLineSeries.LineStyle.Width = 2f;
            pointLineSeries.LineStyle.AntiAliasing = LineAntialias.Normal;
            pointLineSeries.MouseHighlight = MouseOverHighlight.Simple;
            //pointLineSeries.PointsVisible = true;
            pointLineSeries.PointStyle.Color1 = Color.FromArgb(0, 255, 0, 0);
            //pointLineSeries.PointStyle.Color2 = Colors.Red;
            pointLineSeries.PointStyle.Width = 7;
            pointLineSeries.PointStyle.Height = 7;
            pointLineSeries.PointStyle.BorderWidth = 1;

            _chart.ViewXY.PointLineSeries.Add(pointLineSeries);
            //_chart.ViewXY.ZoomToFit();

            //Allow chart rendering
            _chart.EndUpdate();

            gridChart.Children.Add(_chart);

            _chart.MouseMove += new MouseEventHandler(_chart_MouseMove);


        }

        private void CreateChart2()
        {
            if (_chart2 != null)
                _chart2.Dispose();
            // Create a new chart.
            _chart2 = new LightningChartUltimate();
            AxisX xAxis2 = _chart2.ViewXY.XAxes[0];

            //Disable rendering, strongly recommended before updating chart properties
            _chart2.BeginUpdate();

            //Reduce memory usage and increase performance
            _chart2.ViewXY.DropOldSeriesData = false;


            //Don't show legend box
            _chart2.ViewXY.LegendBoxes[0].Visible = false;

            // Configure x-axis.
            //AxisX axisX = _chart2.ViewXY.XAxes[0];
            xAxis2.SetRange(0, 90);
            xAxis2.ValueType = AxisValueType.Number;

            //Y-axis are layered and have common x-axis drawn
            _chart2.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Layered;

            //Remove existing y-axes
            foreach (AxisY yaxis in _chart2.ViewXY.YAxes)
                yaxis.Dispose();
            _chart2.ViewXY.YAxes.Clear();

            //Create new logaritmic y-axis and add to chart
            AxisY yAxis2 = new AxisY(_chart.ViewXY);
            yAxis2.Title.DistanceToAxis = 30;
            yAxis2.SetRange(0, 40);
            yAxis2.Title.Text = "Data";
            yAxis2.Reversed = false;
            yAxis2.LogBase = 10;
            yAxis2.MinorDivCount = 9;
            yAxis2.MajorGrid.Pattern = LinePattern.Solid;
            yAxis2.MinorGrid.Pattern = LinePattern.Solid;
            yAxis2.MinorGrid.Visible = true;
            _chart2.ViewXY.YAxes.Add(yAxis2);

            //Create new PointLineSeries
            PointLineSeries pointLineSeries2 = new PointLineSeries(_chart2.ViewXY, xAxis2, yAxis2);
            pointLineSeries2.MouseInteraction = false;
            pointLineSeries2.Title.Visible = false;
            pointLineSeries2.LineStyle.Color = Colors.Yellow;
            pointLineSeries2.Title.Color = pointLineSeries2.LineStyle.Color;
            pointLineSeries2.LineStyle.AntiAliasing = LineAntialias.Normal;
            pointLineSeries2.MouseHighlight = MouseOverHighlight.Simple;
            pointLineSeries2.PointStyle.Color1 = Color.FromArgb(0, 255, 0, 0);
            pointLineSeries2.PointStyle.Width = 7;
            pointLineSeries2.PointStyle.Height = 7;
            pointLineSeries2.PointStyle.BorderWidth = 1;

            _chart2.ViewXY.PointLineSeries.Add(pointLineSeries2);
            //_chart.ViewXY.ZoomToFit();

            //Allow chart rendering
            _chart2.EndUpdate();

            _chart2.MouseMove += new MouseEventHandler(_chart2_MouseMove);

            gridChart2.Children.Add(_chart2);
        }


        private void CreateChart3()
        {
            if (_chart3 != null)
                _chart3.Dispose();
            // Create a new chart.
            _chart3 = new LightningChartUltimate();
            AxisX xAxis3 = _chart3.ViewXY.XAxes[0];

            //Disable rendering, strongly recommended before updating chart properties
            _chart3.BeginUpdate();

            //Reduce memory usage and increase performance
            _chart3.ViewXY.DropOldSeriesData = false;


            //Don't show legend box
            _chart3.ViewXY.LegendBoxes[0].Visible = false;

            // Configure x-axis.
            //AxisX axisX = _chart2.ViewXY.XAxes[0];
            xAxis3.SetRange(0, 90);
            xAxis3.ValueType = AxisValueType.Number;

            //Y-axis are layered and have common x-axis drawn
            _chart3.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Layered;

            //Remove existing y-axes
            foreach (AxisY yaxis in _chart3.ViewXY.YAxes)
                yaxis.Dispose();
            _chart3.ViewXY.YAxes.Clear();

            //Create new logaritmic y-axis and add to chart
            AxisY yAxis3 = new AxisY(_chart.ViewXY);
            yAxis3.Title.DistanceToAxis = 30;
            yAxis3.SetRange(0, 40);
            yAxis3.Title.Text = "Data";
            yAxis3.Reversed = false;
            yAxis3.LogBase = 10;
            yAxis3.MinorDivCount = 9;
            yAxis3.MajorGrid.Pattern = LinePattern.Solid;
            yAxis3.MinorGrid.Pattern = LinePattern.Solid;
            yAxis3.MinorGrid.Visible = true;
            _chart3.ViewXY.YAxes.Add(yAxis3);

            //Create new PointLineSeries
            PointLineSeries pointLineSeries3 = new PointLineSeries(_chart3.ViewXY, xAxis3, yAxis3);
            pointLineSeries3.MouseInteraction = false;
            pointLineSeries3.Title.Visible = false;
            pointLineSeries3.LineStyle.Color = Colors.Yellow;
            pointLineSeries3.Title.Color = pointLineSeries3.LineStyle.Color;
            pointLineSeries3.LineStyle.AntiAliasing = LineAntialias.Normal;
            pointLineSeries3.MouseHighlight = MouseOverHighlight.Simple;
            pointLineSeries3.PointStyle.Color1 = Color.FromArgb(0, 255, 0, 0);
            pointLineSeries3.PointStyle.Width = 7;
            pointLineSeries3.PointStyle.Height = 7;
            pointLineSeries3.PointStyle.BorderWidth = 1;

            _chart3.ViewXY.PointLineSeries.Add(pointLineSeries3);
            //_chart.ViewXY.ZoomToFit();

            //Allow chart rendering
            _chart3.EndUpdate();

            _chart3.MouseMove += new MouseEventHandler(_chart3_MouseMove);

            gridChart3.Children.Add(_chart3);

        }

        //drawing markers of coordinates
        void _chart_MouseMove(object sender, MouseEventArgs e)
        {
            _chart.BeginUpdate();

            //if (redyToMark)
            //{
            var point = e.GetPosition(_chart);

            double x, y;
            int nearestIndex = 0;
            bool solved = _chart.ViewXY.PointLineSeries[0].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out x, out y, out nearestIndex);

            if (solved)
            //if (_chart.ViewXY.PointLineSeries[0].IsMouseOver((int)point.X, (int)point.Y))
            {
                SeriesEventMarker sem = new SeriesEventMarker(_chart.ViewXY.PointLineSeries[0]);
                sem.XValue = x;
                sem.YValue = y;

                if ((!(prev != null) || (prev.XValue != sem.XValue || prev.YValue != sem.YValue)))
                {
                    var test = ((LightningChartUltimate)sender);

                    sem.Label.Text = x.ToString("0") + " ; " + y.ToString("0");
                    sem.Label.HorizontalAlign = AlignmentHorizontal.Center;
                    sem.Label.Font = new WpfFont("Segoe UI", 11f, true, false);
                    sem.Label.Shadow.ContrastColor = Colors.Black;
                    sem.Label.VerticalAlign = AlignmentVertical.Top;
                    sem.Symbol.Color1 = Colors.Orange;
                    sem.Symbol.Height = 12;
                    sem.Symbol.Width = 12;
                    sem.Symbol.Shape = SeriesMarkerPointShape.Circle;
                    _chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.Add(sem);

                    prev = sem;
                    _chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, _chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.Count - 1);
                }
            }
            //}
            _chart.EndUpdate();
        }


        void _chart2_MouseMove(object sender, MouseEventArgs e)
        {
            _chart2.BeginUpdate();

            //if (redyToMark)
            //{
            var point = e.GetPosition(_chart2);

            double x, y;
            int nearestIndex = 0;
            bool solved = _chart2.ViewXY.PointLineSeries[0].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out x, out y, out nearestIndex);

            if (solved)
            //if (_chart.ViewXY.PointLineSeries[0].IsMouseOver((int)point.X, (int)point.Y))
            {
                SeriesEventMarker sem = new SeriesEventMarker(_chart2.ViewXY.PointLineSeries[0]);
                sem.XValue = x;
                sem.YValue = y;

                if ((!(prev != null) || (prev.XValue != sem.XValue || prev.YValue != sem.YValue)))
                {
                    var test = ((LightningChartUltimate)sender);

                    sem.Label.Text = x.ToString("0") + " ; " + y.ToString("0");
                    sem.Label.HorizontalAlign = AlignmentHorizontal.Center;
                    sem.Label.Font = new WpfFont("Segoe UI", 11f, true, false);
                    sem.Label.Shadow.ContrastColor = Colors.Black;
                    sem.Label.VerticalAlign = AlignmentVertical.Top;
                    sem.Symbol.Color1 = Colors.Orange;
                    sem.Symbol.Height = 12;
                    sem.Symbol.Width = 12;
                    sem.Symbol.Shape = SeriesMarkerPointShape.Circle;
                    _chart2.ViewXY.PointLineSeries[0].SeriesEventMarkers.Add(sem);

                    prev = sem;
                    _chart2.ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, _chart2.ViewXY.PointLineSeries[0].SeriesEventMarkers.Count - 1);
                }
            }
            //}
            _chart2.EndUpdate();
        }


        void _chart3_MouseMove(object sender, MouseEventArgs e)
        {
            _chart3.BeginUpdate();

            //if (redyToMark)
            //{
            var point = e.GetPosition(_chart3);

            double x, y;
            int nearestIndex = 0;
            bool solved = _chart3.ViewXY.PointLineSeries[0].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out x, out y, out nearestIndex);

            if (solved)
            //if (_chart.ViewXY.PointLineSeries[0].IsMouseOver((int)point.X, (int)point.Y))
            {
                SeriesEventMarker sem = new SeriesEventMarker(_chart3.ViewXY.PointLineSeries[0]);
                sem.XValue = x;
                sem.YValue = y;

                if ((!(prev != null) || (prev.XValue != sem.XValue || prev.YValue != sem.YValue)))
                {
                    var test = ((LightningChartUltimate)sender);

                    sem.Label.Text = x.ToString("0") + " ; " + y.ToString("0");
                    sem.Label.HorizontalAlign = AlignmentHorizontal.Center;
                    sem.Label.Font = new WpfFont("Segoe UI", 11f, true, false);
                    sem.Label.Shadow.ContrastColor = Colors.Black;
                    sem.Label.VerticalAlign = AlignmentVertical.Top;
                    sem.Symbol.Color1 = Colors.Orange;
                    sem.Symbol.Height = 12;
                    sem.Symbol.Width = 12;
                    sem.Symbol.Shape = SeriesMarkerPointShape.Circle;
                    _chart3.ViewXY.PointLineSeries[0].SeriesEventMarkers.Add(sem);

                    prev = sem;
                    _chart3.ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, _chart3.ViewXY.PointLineSeries[0].SeriesEventMarkers.Count - 1);
                }
            }
            //}
            _chart3.EndUpdate();
        }

        //drawing points to charts
        private void UpDateChart(List<short>[] testList)
        {

            for (int i = 0; i < numberOfCharts; i++)
            {
                chartArray[i].BeginUpdate();

                chartArray[i].ViewXY.PointLineSeries[0].Points = new SeriesPoint[testList[i].Count];
                for (int j = 0; j < testList[i].Count; j++)
                    chartArray[i].ViewXY.PointLineSeries[0].Points[j] = new SeriesPoint(j, testList[i][j]);

                chartArray[i].EndUpdate();
            }

            CancelIntensity();
        }

        // change linear to Log and vise versa axes
        private void checkBoxLogX_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (_chart == null || _chart2 == null || _chart3 == null)
            {
                return;
            }

            if (checkBoxLogX.IsChecked == true)
            {
                for (int i = 0; i < numberOfCharts; i++)
                    chartArray[i].ViewXY.XAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                for (int i = 0; i < numberOfCharts; i++)
                    chartArray[i].ViewXY.XAxes[0].ScaleType = ScaleType.Linear;
            }
        }

        private void checkBoxLogY_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (_chart == null || _chart2 == null || _chart3 == null)
            {
                return;
            }

            if (checkBoxLogY.IsChecked == true)
            {
                for (int i = 0; i < numberOfCharts; i++)
                    chartArray[i].ViewXY.YAxes[0].ScaleType = ScaleType.Logarithmic;
            }
            else
            {
                for (int i = 0; i < numberOfCharts; i++)
                    chartArray[i].ViewXY.YAxes[0].ScaleType = ScaleType.Linear;
            }
        }


        // Resize all graphic elements
        private void buttonFitView_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < numberOfCharts; i++)
                chartArray[i].ViewXY.ZoomToFit();
        }



        /// <summary>
        /// Dispose items in collection before and clear.
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="list">Collection</param>
#if WpfSemibindable
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#else
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#endif
        {
            if (list == null)
                return;

            foreach (IDisposable item in list)
            {
                if (item != null)
                    item.Dispose();
            }

            list.Clear();
        }


        public void Connected()
        {
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.Green);
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Gray);
        }

        public void Disconnected()
        {
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.Gray);
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Red);
        }

        public void WritedBytes(byte[] bByte)
        {
        }

        private void Button_Disconnect_Click(object sender, RoutedEventArgs e)
        {
            Client.OnConnectNet -= Connected;
            Client.OnDisconnectNet -= Disconnected;
            Client.OnReadByte -= MathFunc;
            Client.OnWriteByte -= WritedBytes;
            // Client.OnTestRequest -=

            Client.Disconnect();
            DisconnectButton.Background = Brushes.Red;
            ConnectButton.Background = Brushes.Gray;
        }



        private void Button_ConnectClick(object sender, RoutedEventArgs e)
        {
            string strIP;
            int testPortOperChannel;
            if (IPTextBox.Text.Equals(""))
            {
                strIP = "127.0.0.1";
                IPTextBox.Text = "127.0.0.1";
            }
            else
            {
                strIP = IPTextBox.Text;
            }

            if (PortTextBox.Text.Equals(""))
            {
                testPortOperChannel = 11111;
                PortTextBox.Text = testPortOperChannel.ToString();
            }
            else
            {
                testPortOperChannel = Convert.ToInt32(PortTextBox.Text);
            }

            if (Client.Connect(strIP, testPortOperChannel))
            {
                Client.OnConnectNet += Connected;
                Client.OnDisconnectNet += Disconnected;
                Client.OnReadByte += MathFunc;
                Client.OnWriteByte += WritedBytes;
                //Client.OnTestRequest +=

                ConnectButton.Background = Brushes.Green;
                DisconnectButton.Background = Brushes.Gray;
            }
            else
            {
                ConnectButton.Background = Brushes.Gray;
                DisconnectButton.Background = Brushes.Red;
            }

        }



        private void TbDelay_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var newValue = Int32.Parse(tbDelay.Text);
                if (newValue >= minDelay && newValue <= maxDelay)
                {
                    _delay = newValue;
                }
            }
            catch
            {
                tbDelay.Text = _delay.ToString();
            }
        }


        private List<short> CreateData()
        {
            var outputData = new List<short>();
            for (int j = 0; j < _numberOfPoints; j++)
            {
                outputData.Add((short)(_random.Next(minYValue, maxYValue)));
            }
            return outputData;
        }

        private async Task WorkTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                var newData = CreateData();
                Dispatcher?.Invoke(() =>
                {
                    UpDateChart(new List<short>[] { newData, newData, newData });
                    //UpDateChart(newData);
                    //newData.Reverse();
                    //UpDateChart2(newData);
                });
                await Task.Delay(_delay);
            }
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            if (_tokenSource != null && !_tokenSource.Token.IsCancellationRequested)
            {
                redyToMark = true;
                _tokenSource.Cancel();
                // button.Content = "Start";
                FitView();
            }
            else
            {
                redyToMark = false;
                _tokenSource = new CancellationTokenSource();
                WorkTask(_tokenSource.Token);
                //button.Content = "Stop";
                for (int i = 0; i < numberOfCharts; i++)
                    chartArray[i].ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, chartArray[i].ViewXY.PointLineSeries[0].SeriesEventMarkers.Count);
                FitView();
            }
        }


        //extracting the root of the sum of squares
        public List<short> MathFunc2(List<short> listRE, List<short> listIM)
        {
            try
            {
                List<short> finalList = new List<short>();
                for (int i = 0; i < listIM.Count(); i++)
                {
                    double temp = Math.Sqrt(Math.Pow(listRE[i], 2) + Math.Pow(listIM[i], 2));
                    finalList.Add((short)temp);
                }


                return finalList;
            }
            catch (Exception exp)
            {
                return new List<short> { 0 };
            }

        }

        //prepare data for charts
        public void MathFunc(byte[] resultBData)
        {
            try
            {
                if (resultBData.Count() >= LEN_HEAD)
                {
                    List<short> partRE = new List<short>();
                    List<short> partIM = new List<short>();
                    List<short> finalList = new List<short>();
                    bool trashFlag = true;

                    short temp;
                    for (int i = 3; i < resultBData.Length - 3; i += 2)
                    {
                        if (trashFlag)
                        {
                            temp = BitConverter.ToInt16(resultBData, i);
                            if (temp < 50 && temp > -50)
                                partRE.Add(temp);
                            trashFlag = false;
                        }
                        else
                        {
                            temp = BitConverter.ToInt16(resultBData, i);
                            if (temp < 50 && temp > -50)
                                partIM.Add(temp);
                            trashFlag = true;
                        }
                    }

                    finalList = MathFunc2(partRE, partIM);

                    Dispatcher?.Invoke(() =>
                    {
                        if (finalList.Count > 1)
                            UpDateChart(new List<short>[] { partRE, partIM, finalList });
                        else
                            UpDateChart(new List<short>[] { partRE, partIM });
                    });

                }
            }
            catch (Exception exp) { }

        }



        CancellationTokenSource ctsI = new CancellationTokenSource();
        int _CancellationTokenIntensityDelay = 80;
        int _RequestIntensityTimer = 10;
        private async void IntensityRequest2(CancellationToken stopToken)
        {
            while (!stopToken.IsCancellationRequested)
            {
                ctsI = new CancellationTokenSource();
                CancellationToken token = ctsI.Token;
                byte[] test = new byte[] { 0x01, 0, 0 };
                // test[3] = COUNTER_CMD_OWN;
                //COUNTER_CMD_OWN++;
                Client.SendBytesVithoutPacket(test);
                try
                {
                    var T = await Task.Delay(_CancellationTokenIntensityDelay, token).ContinueWith(_ => { return 42; });
                }
                catch { }
                await Task.Delay(_RequestIntensityTimer);

            }
        }


        public void CancelIntensity()
        {
            ctsI.Cancel();
        }

        private void FitView()
        {
            for (int i = 0; i < numberOfCharts; i++)
                chartArray[i].ViewXY.ZoomToFit();
        }

        private void Button_Send_Request_Click(object sender, RoutedEventArgs e)
        {
            if (_TesttokenSource != null && !_TesttokenSource.Token.IsCancellationRequested)
            {
                _TesttokenSource.Cancel();
                SendRequestButton.Content = "Send request";
                redyToMark = true;
                FitView();
            }
            else
            {
                _TesttokenSource = new CancellationTokenSource();
                IntensityRequest2(_TesttokenSource.Token);
                SendRequestButton.Content = "Stop";
                redyToMark = false;
                FitView();
            }

            //byte[] test = new byte[] { 0x01, 0, 0 };
            // Client.SendBytesVithoutPacket(test);

        }
        private void TbPoints_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var newValue = (ushort)Int32.Parse(tbPoints.Text);
                if (newValue >= minPoints && newValue <= maxPoints)
                {
                    _numberOfPoints = newValue;
                    _chart.ViewXY.XAxes[0].SetRange(0, _numberOfPoints - 1);
                }
            }
            catch
            {
                tbPoints.Text = _numberOfPoints.ToString();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < numberOfCharts; i++)
                chartArray[i].ViewXY.PointLineSeries[0].Points = new SeriesPoint[0];
        }
    }
}
