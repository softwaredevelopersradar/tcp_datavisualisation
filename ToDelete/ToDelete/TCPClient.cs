﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ToDelete
{

    public struct Packet
    {
        public PacketServiceData packetServiceData;
        public byte[] bData;

        private static ushort test = 2;
    }

    public struct PacketServiceData
    {
        public byte bAdressSender;
        public byte bAdressReceiver;
        public byte bCode;
        public byte bCounter;
        public ushort usLengthInform;
    }


    public class TCPClient
    {
        #region variables

        private Thread thrRead;
        public NetworkStream streamClient;
        private TcpClient tcpClient;
        private byte bAdrOwn = 0;
        private byte bAdrOpponent = 0;
        private byte COUNTER_CMD = 0;

        #endregion

        static int LEN_HEAD = 3;

        #region constants
        const byte CONNECTION_REQUEST_CODE = 9;
        const byte TEST_MESSAGE_CODE = 10;
        const byte TEXT_MESSAGE_APPROVED_CMD_CODE = 11;
        const byte EXEC_BEAR_CODE = 12;
        const byte FRIEQUENCIES_FOR_SUPPRESSION_CODE = 13;
        const byte COORD_REQUEST_CODE = 14;

        const byte VALUE_OF_MESSAGE_APPROVED_TEXT_CMD = 0;

        #endregion



        //************** EVENT **************//      
        public delegate void ConnectEventHandler();
        public event ConnectEventHandler OnConnectNet;
        public event ConnectEventHandler OnDisconnectNet;

        public delegate void ByteEventHandler(byte[] bByte);
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public delegate void CmdConnectionRequestEventHandler(byte bRegime);
        public event CmdConnectionRequestEventHandler OnConnectionRequest;




        // connect to server
        protected virtual void ConnectNet()
        {
            if (OnConnectNet != null)
            {
                OnConnectNet();//Raise the event
            }
        }

        // disconnect 
        protected virtual void DisconnectNet()
        {
            if (OnDisconnectNet != null)
            {
                OnDisconnectNet();
            }
        }

        // read array of byte
        protected virtual void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);
            }

        }

        // write array of byte
        protected virtual void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);
            }
        }

        // read connection request
        protected virtual void ConnectionRequest(byte bRegime)
        {
            if (OnConnectionRequest != null)
            {
                OnConnectionRequest(bRegime);
            }
        }

        // constructor
        public TCPClient(byte AdrOwn, byte AdrOpponent)
        {
            bAdrOwn = AdrOwn;
            bAdrOpponent = AdrOpponent;
        }


        // connect to server
        public bool Connect(string strIPServer, int iPortServer)
        {
            // if there is client
            if (tcpClient != null)
            {
                tcpClient.Close();
            }

            // create client
            tcpClient = new TcpClient();

            try
            {
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (Exception)
            {
                //generate event
                DisconnectNet();
                // logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                return false;
            }


            if (tcpClient.Connected)
            {
                // create stream for client
                if (streamClient != null)
                    streamClient = null;
                streamClient = tcpClient.GetStream();

                // destroy thread for reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // create thread for reading
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }

                catch (System.Exception)
                {

                    // generate event
                    DisconnectNet();
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    return false;

                }
            }

            ConnectNet();
            return true;
        }


        public void Disconnect()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                    streamClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (thrRead != null)
            {
                // destroy thread for reading
                try
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // generate event
            DisconnectNet();
        }

        //private static Logger logger = LogManager.GetCurrentClassLogger();

        //Read and parse  income Data.
        private void ReadData()
        {

            Packet packet = new Packet();

            int curRe;
            int curIm;

            List<int> testListRE = new List<int>();
            List<int> testListIM = new List<int>();

            int iReadLength = 0;
            int currSize = 0;
            while (true)
            {
                try
                {
                    byte[] bRead = null;
                    byte[] buffer = null;
                    byte[] bData = null;

                    Array.Resize(ref buffer, LEN_HEAD);
                    Array.Resize(ref bRead, 0);
                    currSize = 0;
                    iReadLength = streamClient.Read(buffer, 0, LEN_HEAD);
                    //ReadByte(buffer);
                    byte a = buffer[1];
                    byte b = buffer[2];
                    byte[] test = { b, a };
                    ushort realMessageLength = BitConverter.ToUInt16(test, 0);
                    //ushort realMessageLength = BitConverter.ToUInt16(buffer, 1);

                    Array.Resize(ref bRead, realMessageLength + LEN_HEAD);
                    Array.Resize(ref buffer, realMessageLength + LEN_HEAD);
                    //new
                    Array.Copy(buffer, 0, bRead, 0, LEN_HEAD);
                    currSize += iReadLength;

                    while (currSize < bRead.Length)
                    {
                        iReadLength = streamClient.Read(buffer, 0, bRead.Length - currSize);

                        Array.Copy(buffer, 0, bRead, currSize, iReadLength);
                        currSize += iReadLength;

                        if (iReadLength == 0)
                        {
                            Disconnect();
                        }
                    }
                    if (bRead.Length > 0)
                    {
                        //// событие приема байт
                        ReadByte(bRead);

                        try
                        {
                            if (bRead.Length >= LEN_HEAD)
                            {

                                //packet.packetServiceData.bAdressSender = bRead[0];
                                //packet.packetServiceData.bAdressReceiver = bRead[1];
                                packet.packetServiceData.bCode = bRead[0];
                                //packet.packetServiceData.bCounter = bRead[3];
                                // REVERSE THAT BITES
                                packet.packetServiceData.usLengthInform = BitConverter.ToUInt16(bRead, 1);

                                Array.Resize(ref bData, bRead.Length - LEN_HEAD);
                                Array.Copy(bRead, LEN_HEAD, bData, 0, bRead.Length - LEN_HEAD);
                                packet.bData = bData;

                                switch (packet.packetServiceData.bCode)
                                {
                                    case CONNECTION_REQUEST_CODE:
                                        byte bRegime = packet.bData[0];
                                        ConnectionRequest(bRegime);
                                        break;
                                    case TEST_MESSAGE_CODE:
                                        byte btest = packet.bData[0];
                                        ConnectionRequest(btest);
                                        //
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    DisconnectNet();
                    return;
                }
            }
        }

        //example of codogramm(send something)
        public void SendData(Packet packetToSend)
        {
            byte[] bSend;
            byte[] bHead = Serializer.StructToByteArray(packetToSend.packetServiceData);
            if (packetToSend.bData != null)
            {
                bSend = new byte[bHead.Length + packetToSend.bData.Length];
                Array.Copy(bHead, 0, bSend, 0, bHead.Length);
                Array.Copy(packetToSend.bData, 0, bSend, LEN_HEAD, packetToSend.bData.Length);
            }
            else
                bSend = new byte[bHead.Length];
            Array.Copy(bHead, 0, bSend, 0, bHead.Length);


            if (COUNTER_CMD == 255)
            {
                COUNTER_CMD = 0;
            }

            WriteData(bSend);

        }

        //Write bytes to port.
        private bool WriteData(byte[] bSend)
        {
            try
            {
                streamClient.Write(bSend, 0, bSend.Length);
                OnWriteByte(bSend);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public void SendBytesVithoutPacket(byte[] bSend)
        {
            try
            {
                // byte[] bSend = Serializer.StringToByteArray(strText);
                WriteData(bSend);
            }
            catch (System.Exception)
            {
                //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }
    }
}
