﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ToDelete
{
    public class Serializer
    {

        // function of serialization of structure to the bytes array
        public static byte[] StructToByteArray(object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure.GetType());

            //sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            byte[] outArray = new byte[sizeInBytes];

            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);
            System.Runtime.InteropServices.Marshal.StructureToPtr(structure, ptr, false);
            System.Runtime.InteropServices.Marshal.Copy(ptr, outArray, 0, sizeInBytes);
            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

            return outArray;
        }

        // function of serialization of bytes array in structure
        public static void ByteArrayToStructure(byte[] bytes, ref object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, ptr, sizeInBytes);
            structure = System.Runtime.InteropServices.Marshal.PtrToStructure(ptr, structure.GetType());

            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

        }

        public static string ByteArrayToString(byte[] bytes)
        {
            char curFirstChar;
            char curSecondChar;


            string hex = BitConverter.ToString(bytes);
            hex = hex.Replace("-", "");

            char[] charArray = hex.ToCharArray();

            for (int i = 0; i + 4 <= charArray.Length; i += 4)
            {
                curFirstChar = charArray[i];
                curSecondChar = charArray[i + 1];


                charArray[i] = charArray[i + 3];
                charArray[i + 1] = charArray[i + 2];
                charArray[i + 2] = curFirstChar;
                charArray[i + 3] = curSecondChar;

                charArray[i + 1] = charArray[i + 2];
                charArray[i + 2] = curFirstChar;
                charArray[i + 3] = curSecondChar;


            }
            string finalHex = new string(charArray);


            finalHex = string.Join(" ", Regex.Matches(finalHex, ".{4}").Cast<Match>());
            if (finalHex.Length >= 160)
                finalHex = string.Join("\r\n", Regex.Matches(finalHex, ".{160}").Cast<Match>());
            //return hex.Replace("-", "");
            return finalHex;
        }

        public static byte[] StringToByteArray(string hex)
        {
            hex = hex.Replace(" ", "");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;


        }
    }
}
