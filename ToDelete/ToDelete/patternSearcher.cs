﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDelete
{
    static class PatternSearcher
    {
        static readonly List<int> Empty = new List<int>();

        public static List<int> Locate(this List<byte> self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate))
                return Empty;

            var list = new List<int>();

            for (int i = 0; i < self.Count(); i++)
            {
                if (!IsMatch(self, i, candidate))
                    continue;

                list.Add(i);
            }

            return list.Count == 0 ? Empty : list;
        }

        static bool IsMatch(List<byte> dataList, int position, byte[] candidate)
        {
            if (candidate.Length > (dataList.Count() - position))
                return false;

            for (int i = 0; i < candidate.Length; i++)
                if (dataList[position + i] != candidate[i])
                    return false;

            return true;
        }

        static bool IsEmptyLocate(List<byte> dataList, byte[] candidate)
        {
            return dataList == null
                || candidate == null
                || dataList.Count() == 0
                || candidate.Length == 0
                || candidate.Length > dataList.Count();
        }

    }
}
